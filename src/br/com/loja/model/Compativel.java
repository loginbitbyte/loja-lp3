package br.com.loja.model;

public class Compativel {

	private Integer id_software;
	private Integer id_sistema;
	private String Sistema;
	private String Software;

	
	public String getSoftware() {
		return Software;
	}
	public void setSoftware(String nomeSofware) {
		this.Software = nomeSofware;
	}
	public String getSistema() {
		return Sistema;
	}
	public void setSistema(String nomeSistema) {
		this.Sistema = nomeSistema;
	}
	
	public Integer getId_software() {
		return id_software;
	}
	public void setId_software(Integer id_software) {
		this.id_software = id_software;
	}
	public Integer getId_sistemas() {
		return id_sistema;
	}
	public void setId_sistemas(Integer id_sistema) {
		this.id_sistema = id_sistema;
	}	
	
}
