package br.com.loja.controller;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import br.com.loja.dao.FornecedorDao;
import br.com.loja.dao.HardwareDao;
import br.com.loja.model.Fornecedor;
import br.com.loja.model.Hardware;


public class AddHardware extends HttpServlet {
	private static final long serialVersionUID = 1L;


	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
			
		

		
		//Adicionando os atibutos a hardware que herda de produto tendo nome,descricao,preco
		
        String nome = request.getParameter("nome");
        String descricao = request.getParameter("descricao");
        String preco = request.getParameter("preco");
        String idFornecedor = request.getParameter("idFornecedor");
        String modelo = request.getParameter("modelo");
        String fabricante = request.getParameter("fabricante");
        
		//Cria��o do Obj hardware recebendo os parametros de add-hardware.jsp
		Hardware obj = new Hardware();
		
        obj.setNome(nome);
        obj.setDescricao(descricao);
        obj.setPreco(Double.parseDouble(preco));
        obj.setId_fornecedor(Integer.parseInt(idFornecedor));
        obj.setModelo(modelo);
        obj.setFabricante(fabricante);
        
        HardwareDao dao = new HardwareDao();
        dao.adiciona(obj);
        
        String msg = "<br>O Hardware " + nome + " foi adicionado com sucesso.";
        
        request.setAttribute("msg", msg);
		
      //Carrega os resultados da requisi��o em uma p�gina especificas
		RequestDispatcher rd = request.getRequestDispatcher("index.jsp");
		  rd.forward(request, response);
        
    }

		
		
	}


