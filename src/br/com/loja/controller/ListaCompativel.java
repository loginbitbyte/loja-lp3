package br.com.loja.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import br.com.loja.dao.CompativelDao;
import br.com.loja.dao.SistemasDao;
import br.com.loja.model.Compativel;
import br.com.loja.model.Sistemas;


public class ListaCompativel extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		//Inst�ncia dos objetos que realizam a busca no banco
		CompativelDao dao = new CompativelDao();

		List<Compativel> lista = dao.getLista();
		
		request.setAttribute("listaCompativel", lista);
		
		
		//Carrega os resultados da requisi��o em uma p�gina especificas
		RequestDispatcher rd = request.getRequestDispatcher("lista-compativel.jsp");
		  rd.forward(request, response);
	}

}
