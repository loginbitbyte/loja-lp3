package br.com.loja.controller;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import br.com.loja.dao.SoftwareDao;
import br.com.loja.dao.HardwareDao;
import br.com.loja.model.Software;


public class AddSoftware extends HttpServlet {
	private static final long serialVersionUID = 1L;


	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
			
		
		//Cria��o do Obj hardware recebendo os parametros de add-hardware.jsp
		Software obj = new Software();
		
		//Adicionando os atibutos a hardware que herda de produto tendo nome,descricao,preco
		
        String nome = request.getParameter("nome");
        String descricao = request.getParameter("descricao");
        String preco = request.getParameter("preco");
        String idFornecedor = request.getParameter("idFornecedor");
        String versao = request.getParameter("versao");
        
       
        obj.setNome(nome);
        obj.setDescricao(descricao);
        obj.setPreco(Double.parseDouble(preco));
        obj.setId_fornecedor(Integer.parseInt(idFornecedor));
        obj.setVersao(Integer.parseInt(versao));
  
        
        SoftwareDao dao = new SoftwareDao();
        dao.adiciona(obj);
        
        String msg = "<br>O Software " + nome + " foi adicionado com sucesso.";
        
        request.setAttribute("msg", msg);
		
      //Carrega os resultados da requisi��o em uma p�gina especificas
		RequestDispatcher rd = request.getRequestDispatcher("index.jsp");
		  rd.forward(request, response);
        
    }

		
		
	}


