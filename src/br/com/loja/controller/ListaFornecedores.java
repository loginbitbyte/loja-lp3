package br.com.loja.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import br.com.loja.dao.FornecedorDao;
import br.com.loja.model.Fornecedor;


public class ListaFornecedores extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		FornecedorDao dao = new FornecedorDao();
		
		List<Fornecedor> lista = dao.getLista();
		
		request.setAttribute("listaFornecedores", lista);
		
		
		//Carrega os resultados da requisi��o em uma p�gina especificas
		RequestDispatcher rd = request.getRequestDispatcher("lista-fornecedores.jsp");
		  rd.forward(request, response);
	}

}
