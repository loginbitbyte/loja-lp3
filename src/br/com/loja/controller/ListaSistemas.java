package br.com.loja.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import br.com.loja.dao.SistemasDao;
import br.com.loja.model.Sistemas;


public class ListaSistemas extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		SistemasDao dao = new SistemasDao();
		
		List<Sistemas> lista = dao.getLista();
		
		request.setAttribute("listaSistemas", lista);
		
		//Carrega os resultados da requisi��o em uma p�gina especificas
		RequestDispatcher rd = request.getRequestDispatcher("lista-sistemas.jsp");
		  rd.forward(request, response);
	}

}
