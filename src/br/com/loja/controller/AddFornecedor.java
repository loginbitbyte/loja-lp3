package br.com.loja.controller;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import br.com.loja.dao.FornecedorDao;
import br.com.loja.model.Fornecedor;


public class AddFornecedor extends HttpServlet {
	private static final long serialVersionUID = 1L;


	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
			
		//Objeto que envia dados ao DAO, para realizar alguma opera��o no Banco de Dados
		FornecedorDao dao = new FornecedorDao();
		//Objeto Fornecedor
		Fornecedor obj = new Fornecedor();
		
		//Recebe os valores preenchidos no formulario enviado atrav�s do objeto HttpServletRequest
        String nome = request.getParameter("nome");
        String numero = request.getParameter("numero");
        
        obj.setNome(nome);
        obj.setNumero(Integer.parseInt(numero));
        
        //Envio do objeto para ser adicionado ao banco
        dao.adiciona(obj);
        
        String msg = "<br>O Fornecedor " + nome + " de numero " + numero + " foi adicionado com sucesso.";
        
        PrintWriter pw = response.getWriter();
        pw.println(msg);
        
        request.setAttribute("msg", msg);
		
//        //Carregamento das respostas em uma p�gina especificas
//		RequestDispatcher rd = request.getRequestDispatcher("index.jsp");
//		  rd.forward(request, response);
        
    }

		
		
	}


