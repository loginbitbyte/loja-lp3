package br.com.loja.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


import br.com.loja.dao.SoftwareDao;
import br.com.loja.model.Software;


public class ListaSoftware extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		SoftwareDao dao = new SoftwareDao();
		
		List<Software> lista = dao.getLista();
		
		request.setAttribute("listaSoftware", lista);
		
		//Carrega os resultados da requisi��o em uma p�gina especificas
		RequestDispatcher rd = request.getRequestDispatcher("lista-software.jsp");
		  rd.forward(request, response);
	}

}
