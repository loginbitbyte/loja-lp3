package br.com.loja.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


import br.com.loja.dao.HardwareDao;
import br.com.loja.model.Hardware;


public class ListaHardware extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HardwareDao dao = new HardwareDao();
		
		List<Hardware> lista = dao.getLista();
		
		request.setAttribute("listaHardware", lista);
		
		//Carrega os resultados da requisi��o em uma p�gina especificas
		RequestDispatcher rd = request.getRequestDispatcher("lista-hardware.jsp");
		  rd.forward(request, response);
	}

}
