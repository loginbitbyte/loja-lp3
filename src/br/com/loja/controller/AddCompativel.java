package br.com.loja.controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import br.com.loja.dao.CompativelDao;
import br.com.loja.model.Compativel;


public class AddCompativel extends HttpServlet {
	private static final long serialVersionUID = 1L;


	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
			
		//Pegar compativel DAO		
		CompativelDao dao = new CompativelDao();
		Compativel obj = new Compativel();
		
        String idSoftware = request.getParameter("id_software");
        String idSistemas = request.getParameter("id_sistemas");
     
        
        obj.setId_sistemas(Integer.parseInt(idSistemas));
        obj.setId_software(Integer.parseInt(idSoftware));
               
        dao.adiciona(obj);
        
        String msg = "<br>A compatibilidade foi adicionado com sucesso.";
        
        request.setAttribute("msg", msg);
		
      //Carrega os resultados da requisi��o em uma p�gina especificas
		RequestDispatcher rd = request.getRequestDispatcher("index.jsp");
		  rd.forward(request, response);
        
    }

		
		
	}


