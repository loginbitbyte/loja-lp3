package br.com.loja.controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


import br.com.loja.dao.SistemasDao;
import br.com.loja.model.Sistemas;


public class AddSistemas extends HttpServlet {
	private static final long serialVersionUID = 1L;


	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
			
		
		SistemasDao dao = new SistemasDao();
		Sistemas obj = new Sistemas();
		
        String nome = request.getParameter("nome");
        String versao = request.getParameter("versao");
        String ano = request.getParameter("ano");
        
        obj.setNome(nome);
        obj.setVersao(versao);
        obj.setAno(Integer.parseInt(ano));
        
        dao.adiciona(obj);
        
        String msg = "<br>O Sistema " + nome +" foi adicionado com sucesso.";
        
        request.setAttribute("msg", msg);
		
      //Carrega os resultados da requisi��o em uma p�gina especificas
		RequestDispatcher rd = request.getRequestDispatcher("index.jsp");
		  rd.forward(request, response);
        
    }

		
		
	}


