package br.com.loja.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

import br.com.loja.fabrica.ConnectionFactory;
import br.com.loja.model.Software;


public class SoftwareDao {
private Connection connection;
	
	//F�brica de Conex�o - Inst�ncia de um nova abertura de conex�o com o banco de dados
	public SoftwareDao(){
		this.connection = new ConnectionFactory().getConnection();
 
	}
	
	public void adiciona(Software software){
		
		//Inst�ncia do objeto que realizam a busca no banco
		ProdutoDao dao = new ProdutoDao();
		
		//Recebe o id Produto ao ser adicionado na tabela
		int idProduto = dao.adiciona(software);
		
		//Query
		String sql = "INSERT into software" +
					 "(versao,id_produto)" +
					 "values(?,?)";
		
		PreparedStatement stmt;
		try {
			stmt = connection.prepareStatement(sql);
			
			stmt.setInt(1,software.getVersao());
			stmt.setInt(2,idProduto);

			stmt.execute();
			stmt.close();
			
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
		
	}
	
	public Software getSoftware(int id){
		try {
			String sql = "select * from sofware where id = ?";
			Software obj = null;
			
			PreparedStatement stmt;
			stmt = connection.prepareStatement(sql);
			ResultSet rs = stmt.executeQuery();
			
			while(rs.next()){
				//Criando o Objeto do tipo Software para receber o resultSet.
				
				obj = new Software();
				
	
				obj.setNome(rs.getString("nome"));
				obj.setPreco(rs.getDouble("preco"));
				obj.setDescricao(rs.getString("descricao"));
				obj.setVersao(rs.getInt("versao"));
				obj.setId_fornecedor(rs.getInt("id_fornecedor"));
				
				
			}
			
			rs.close();
			stmt.close();
			return obj;
			
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
	}
	
	public List<Software> getLista(){
		try {
			
			FornecedorDao forn = new FornecedorDao();
			
			String sql = "select h.id, p.nome,p.descricao,p.preco,h.versao, p.id_fornecedor from produto p JOIN software h on p.id = h.id_produto";
			List<Software> lista = new ArrayList<Software>();
			
			PreparedStatement stmt;
			stmt = connection.prepareStatement(sql);
			ResultSet rs = stmt.executeQuery();
			
			while(rs.next()){
				//Criando o Objeto do tipo Software para receber o resultSet.
				
				Software obj = new Software();
				
				obj.setId(rs.getInt("id"));
				obj.setNome(rs.getString("nome"));
				obj.setPreco(rs.getDouble("preco"));
				obj.setDescricao(rs.getString("descricao"));
				obj.setVersao(rs.getInt("versao"));
				obj.setId_fornecedor(rs.getInt("id_fornecedor"));
				obj.setFornecedor(forn.getFornecedor(rs.getInt("id_fornecedor")));
				
				lista.add(obj);
				
			}
			
			rs.close();
			stmt.close();
			return lista;
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
		
		
		
	}
	
	
	
}