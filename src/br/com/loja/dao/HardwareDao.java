package br.com.loja.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import br.com.loja.fabrica.ConnectionFactory;
import br.com.loja.model.Hardware;


public class HardwareDao {
	
private Connection connection;
	
	//F�brica de Conex�o - Inst�ncia de um nova abertura de conex�o com o banco de dados
	public HardwareDao(){
		this.connection = new ConnectionFactory().getConnection();
	}
	
	public void adiciona(Hardware hardware){
		
		ProdutoDao dao = new ProdutoDao();
		int idProduto = dao.adiciona(hardware);
		

		
		String sql = "INSERT into hardware" +
					 "(modelo,fabricante,id_produto)" +
					 "values(?,?,?)";
		
		PreparedStatement stmt;
		try {
			stmt = connection.prepareStatement(sql);
			
			stmt.setString(1,hardware.getModelo());
			stmt.setString(2,hardware.getFabricante());
			stmt.setInt(3, idProduto);
			
			stmt.execute();
			stmt.close();
			
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
		
	}
	
	
	public List<Hardware> getLista(){
		try {
			
			FornecedorDao forn = new FornecedorDao();
			
			String sql = "select p.nome,p.preco,p.descricao,h.modelo, h.fabricante, p.id_fornecedor from produto p JOIN hardware h on p.id = h.id_produto ";
			List<Hardware> lista = new ArrayList<Hardware>();
			
			PreparedStatement stmt;
			stmt = connection.prepareStatement(sql);
			ResultSet rs = stmt.executeQuery();
			
			while(rs.next()){
				//Criando o Objeto do tipo Software para receber o resultSet.
				
				Hardware obj = new Hardware();
				obj.setNome(rs.getString("nome"));
				obj.setPreco(rs.getDouble("preco"));
				obj.setDescricao(rs.getString("descricao"));
				obj.setModelo(rs.getString("modelo"));
				obj.setFabricante(rs.getString("fabricante"));
				obj.setId_fornecedor(rs.getInt("id_fornecedor"));
				obj.setFornecedor(forn.getFornecedor(rs.getInt("id_fornecedor")));
				
				lista.add(obj);
				
			}
			
			rs.close();
			stmt.close();
			return lista;
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
		
		
		
	}

	
}
