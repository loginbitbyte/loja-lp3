package br.com.loja.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import br.com.loja.fabrica.ConnectionFactory;
import br.com.loja.model.Fornecedor;


public class FornecedorDao {
	
	private Connection connection;
	
	//F�brica de Conex�o - Inst�ncia de um nova abertura de conex�o com o banco de dados
	public FornecedorDao(){
		this.connection = new ConnectionFactory().getConnection();
 
	}
	
	public void adiciona(Fornecedor fornecedor){
		
		//Query
		String sql = "INSERT into fornecedor" +
					 "(numero,nome)" +
					 "values(?,?)";
		
		PreparedStatement stmt;
		try {
			stmt = connection.prepareStatement(sql);
			
			stmt.setInt(1,fornecedor.getNumero());
			stmt.setString(2,fornecedor.getNome());

			stmt.execute();
			stmt.close();
			
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
		
	}
	
	public Fornecedor getFornecedor(int id){
		try {
			String sql = "select * from fornecedor where id = ?";
			Fornecedor obj = null;
			
			PreparedStatement stmt;
			stmt = connection.prepareStatement(sql);
			stmt.setInt(1, id);
			
			ResultSet rs = stmt.executeQuery();
			
			while(rs.next()){
				//Criando o Objeto do tipo Software para receber o resultSet.
				
				obj = new Fornecedor();
				obj.setId(rs.getInt("id"));
				obj.setNome(rs.getString("nome"));
				obj.setNumero(rs.getInt("numero"));
				
			}
			
			rs.close();
			stmt.close();
			return obj;
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
		
		
		
	}
	
	
	public List<Fornecedor> getLista(){
		try {
			String sql = "select * from fornecedor";
			List<Fornecedor> lista = new ArrayList<Fornecedor>();
			
			PreparedStatement stmt;
			stmt = connection.prepareStatement(sql);
			ResultSet rs = stmt.executeQuery();
			
			while(rs.next()){
				//Criando o Objeto do tipo Software para receber o resultSet.
				
				Fornecedor obj = new Fornecedor();
				obj.setId(rs.getInt("id"));
				obj.setNome(rs.getString("nome"));
				obj.setNumero(rs.getInt("numero"));
				
				
				lista.add(obj);
				
			}
			
			rs.close();
			stmt.close();
			return lista;
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
		
		

	}
	
	
	
}
