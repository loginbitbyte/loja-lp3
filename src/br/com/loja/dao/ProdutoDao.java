package br.com.loja.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JOptionPane;

import br.com.loja.fabrica.ConnectionFactory;
import br.com.loja.model.Hardware;
import br.com.loja.model.Produto;
import br.com.loja.model.Software;

public class ProdutoDao {
private Connection connection;
	

	
	
	//Construtor DAO que recebe a conex�o
	public ProdutoDao(){
		this.connection = new ConnectionFactory().getConnection();
 
	}
	
	public int adiciona(Produto produto){
		int idProduto = 0;
		
		String sql = "INSERT into produto" +
					 "(descricao,preco,nome,id_fornecedor)" +
					 "values(?,?,?,?)";
		
		PreparedStatement stmt;
		try {
			stmt = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
			
			stmt.setString(1,produto.getDescricao());
			stmt.setDouble(2,produto.getPreco());
			stmt.setString(3,produto.getNome());
			stmt.setInt(4, produto.getId_fornecedor());
			
			
			stmt.executeUpdate();
			
			ResultSet key = stmt.getGeneratedKeys(); 
			
			if(key.next()){
				idProduto = key.getInt(1);
			}
			
			stmt.close();
			
			return idProduto;	
			
			
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
		
	}
	
	public List<Produto> getLista(){
		try {
						
			String sql = "select * from produto";
			List<Produto> lista = new ArrayList<Produto>();
			
			PreparedStatement stmt;
			stmt = connection.prepareStatement(sql);
			ResultSet rs = stmt.executeQuery();
			
			while(rs.next()){
				//Criando o Objeto do tipo Software para receber o resultSet.
				
				Produto obj = new Produto();
				obj.setId(rs.getInt("id"));
				obj.setNome(rs.getString("nome"));
				obj.setPreco(rs.getDouble("preco"));
				obj.setDescricao(rs.getString("descricao"));
				obj.setId_fornecedor(rs.getInt("id_fornecedor"));
				lista.add(obj);
				
			}
			
			rs.close();
			stmt.close();
			return lista;
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
		
		
		
	}
	
	/*public void adicionaSoftware(Produto objProduto, Software objSoftware){
		
		adiciona(objProduto);
		
		objSoftware.setId_produto(idProduto);
		SoftwareDao dao = new SoftwareDao();
		
		dao.adiciona(objSoftware);
		//Recebe obj produto
		// adicionar em produto
		
		// Invoca o metodo que adiciona software
		// Iria pegar o ID produto e passar como chave
		// 
		
	}
	
public void adicionaHardware(Produto objProduto, Hardware objHardware){
		
		adiciona(objProduto);
		
		objHardware.setId_produto(idProduto);
		HardwareDao dao = new HardwareDao();
		
		dao.adiciona(objHardware);
		//Recebe obj produto
		// adicionar em produto
		
		// Invoca o metodo que adiciona software
		// Iria pegar o ID produto e passar como chave
		// 
		
	}*/
	
}
