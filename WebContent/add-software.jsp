<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
</head>
<body>
	
	<c:import url="header.jsp" />
	<jsp:useBean id="dao" class="br.com.loja.dao.FornecedorDao"/>
	
	<center><table><tr><td><h1>Adicionar Software</h1></td></tr></table><br></center>
	
	<center>
	<form method="get" action="addsoftware">
	<b>Nome:</b>
	<input type="text" name="nome" size="40" class="form-control table-width">

<b>Descricao: </b>
<input type="text" name="descricao" size="40" class="form-control table-width">
		
		<b>Preco: </b> 
		<input type="text" name="preco" size="40" class="form-control table-width">
		
		
		 
		<b>Fornecedor: </b>
		 <br/>
		  <select name="idFornecedor" >
		 <c:forEach var="obj" items="${dao.lista}">
		    
		      <option value=${obj.id}>${obj.nome}</option>
	    	 
	  	 </c:forEach>
		 </select>
		 <br/>
		  <b>Versao: </b>
		  <input type="text" name="versao" size="40" class="form-control table-width">
			<p align="center">&nbsp;<p align="center">
			<input type="submit" value="Enviar" class="btn btn-success">
			<input type="reset" value="Limpar" class="btn btn-warning">

	</table>
	</form></center>
	
	<c:import url="footer.jsp" />
</body>
</html>