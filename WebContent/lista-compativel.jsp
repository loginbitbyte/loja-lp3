<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
</head>
<body>

	<c:import url="header.jsp" />
	 <center>
	 <h2>Lista de Compatibilidades</h2>
      <table class="table table-hover table-striped table-width">
	 	<tr>
	 		<td>Nome Sistema</td>
	 		<td>-</td>
	 		<td>Nome Software</td>
	 		
	 	</tr>
	 	
	  <c:forEach var="obj" items="${listaCompativel}">
			 <tr>
				 <td><b>${obj.software}</b></td>
				 <td>compativel</td>
				 <td><b>${obj.sistema} </b></td>
	      </ul>	
	      
	      	
	  </c:forEach>
	</table>
	
	<c:import url="footer.jsp" />
		
</body>
</html>