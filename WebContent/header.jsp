<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">

<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="css/bootstrap.min.css">
<link rel="stylesheet" href="css/style.css">

<!-- jQuery library -->
<script src="js/jquery.js"></script>

<!-- Latest compiled JavaScript -->
<script src="js/bootstrap.min.js"></script>



<title>Trabalho Lp3</title>
</head>
<body>
	<center>
	<img src="<c:url value="/img/jsp_logo.jpg"/>"/>
	</center>
		<hr />	
		<center>
		<div class="box_dropdow">
			<div class="dropdown">
			  <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
			    Listas
			    <span class="caret"></span>
			  </button>
			  <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
			    <li><a href="fornecedores">Listar Fornecedores</a></li>
			    <li><a href="software">Listar Software</a></li>
			    <li><a href="hardware">Listar Hardware</a></li>
			    <li><a href="sistemas">Listar Sistemas</a></li>
			    <li><a href="compativel">Listar Compativel</a></li>
			  </ul>
			  </div>
			  
			  <div class="dropdown">
			  <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
			    Adicionar
			    <span class="caret"></span>
			  </button>
			  <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
			    <li><a href="add-fornecedor.jsp">Adicionar Fornecedor</a></li>	
					<li><a href="add-hardware.jsp">Adicionar Hardware</a></li>		
					<li><a href="add-software.jsp">Adicionar Software</a></li>
					<li><a href="add-sistemas.jsp">Adicionar Sistemas</a></li>
					<li><a href="add-compativel.jsp">Adicionar Compatibilidade</a></li>
			  </ul>
			</div>
		</div>	
		  </center>
		<hr />		
	
</body>
</html>