<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>

	<c:import url="header.jsp" />
	<jsp:useBean id="dao" class="br.com.loja.dao.ProdutoDao"/>

	<table border=1px>
	  <!-- percorre contatos montando as linhas da tabela -->
	  <tr>	
	  	<td style="solid">ID</td>
	      <td>Nome</td>
	      <td>Descricao</td>
	      <td>preco</td>
	  </tr>
	  <c:forEach var="produto" items="${dao.lista}">
	    <tr>
	      <td>${produto.id}</td>
	      <td>${produto.nome}</td>
	      <td>${produto.descricao}</td>
	      <td>${produto.preco}</td>
	      
	    </tr>
	  </c:forEach>
	</table>
	
	
	<c:import url="footer.jsp" />
		
</body>
</html>