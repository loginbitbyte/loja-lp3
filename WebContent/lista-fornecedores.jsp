<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
</head>
<body>

	<c:import url="header.jsp" />
	
	<%-- <jsp:useBean id="dao" class="br.com.loja.dao.FornecedorDao"/> --%>
	<center>
	<h2>Lista de Fornecedores</h2>
	<table class="table table-hover table-striped table-width" >
	  <!-- percorre contatos montando as linhas da tabela -->
	  <tr>	
	  	<td style="solid">ID</td>
	      <td>Numero</td>
	      <td>Nome</td>
	      
	  </tr>
	  <c:forEach var="obj" items="${listaFornecedores}">
	    <tr>
	      <td>${obj.id}</td>
	      <td>${obj.nome}</td>
	      <td>${obj.numero}</td>
	      
	    </tr>
	  </c:forEach>
	</table>
	</center>
	
	<c:import url="footer.jsp" />
	<c:import url="http://localhost:3000/" />
		
	
</body>
</html>