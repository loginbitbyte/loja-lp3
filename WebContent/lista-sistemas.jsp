<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
</head>
<body>

	<c:import url="header.jsp" />
<center>
<h2>Lista de Sistemas</h2>
	<table class="table table-hover table-striped table-width">
	  <!-- percorre contatos montando as linhas da tabela -->
	  <tr>
	      <td>Nome</td>
	      <td>Versao</td>	  
	      <td>ano</td>
	  </tr>
	  
	  
	  <c:forEach var="obj" items="${listaSistemas}">
	    <tr>
	      <td>${obj.nome}</td>
	      <td>${obj.versao}</td>
	      <td>${obj.ano}</td>
	    </tr>
	  </c:forEach>
	</table>
	</center>
	
	<c:import url="footer.jsp" />
		
</body>
</html>