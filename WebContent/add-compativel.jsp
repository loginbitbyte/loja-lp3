<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
	<c:import url="header.jsp" />
		<jsp:useBean id="dao" class="br.com.loja.dao.SistemasDao"/>
		<jsp:useBean id="soft" class="br.com.loja.dao.SoftwareDao"/>
		
	<center><table><tr><td><h1>Adicionar Compatibilidade</h1></td></tr></table><br></center>
	
	<center>
	<form method="get" action="addcompativel">
	<b>ID Sistema: </b>
		 
		 
		  <select name="id_sistemas" >
		 <c:forEach var="obj" items="${dao.lista}">
		    
		      <option value=${obj.id}>${obj.nome}</option>
	    	 
	  	 </c:forEach>
		 </select>
		
		 
		 <br/>
		 <b>ID Software: </b>


		  <select name="id_software" >
		 <c:forEach var="obj" items="${soft.lista}">
		    
		      <option value=${obj.id} > ${obj.nome}</option>
	    	 
	  	 </c:forEach>
		 </select>
		 
		 <br/> <br/>
		
		
		
		<input type="submit" value="Enviar" class="btn btn-success">
		<input type="reset" value="Limpar" class="btn btn-warning">

	
	</form></center>
	
	<c:import url="footer.jsp" />
</body>
</html>